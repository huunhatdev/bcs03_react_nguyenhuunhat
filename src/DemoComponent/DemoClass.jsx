import React, { Component } from "react";
import style from "./demoStyle.module.css";

export default class DemoClass extends Component {
  render() {
    return <button className={style.colorClass}>DemoClass</button>;
  }
}
