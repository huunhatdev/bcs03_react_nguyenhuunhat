import React, { Component } from "react";

export default class BaiTapChonXe extends Component {
  state = {
    urlImg: "./img/car/red-car.jpg",
    imgArr: [
      "./img/car/red-car.jpg",
      "./img/car/black-car.jpg",
      "./img/car/silver-car.jpg",
    ],
  };

  handleChangeCar = (url) => {
    this.setState({
      urlImg: url,
    });
  };

  render() {
    return (
      <div className="container mt-3">
        <div className="row">
          <div className="col-6">
            <img className="img-fluid" src={this.state.urlImg} alt="" />
          </div>
          <div className="col-6">
            <button
              className="btn btn-danger mx-2"
              onClick={() => {
                this.handleChangeCar("./img/car/red-car.jpg");
              }}
            >
              Red
            </button>
            <button
              className="btn btn-dark mx-2"
              onClick={() => {
                this.handleChangeCar("./img/car/black-car.jpg");
              }}
            >
              Black
            </button>
            <button
              className="btn btn-secondary mx-2"
              onClick={() => {
                this.handleChangeCar("./img/car/silver-car.jpg");
              }}
            >
              Silver
            </button>
          </div>
        </div>

        <div className="row mt-4">
          {this.state.imgArr.map((url) => {
            return <img className="col-4 img-fluid" src={url}></img>;
          })}
        </div>
      </div>
    );
  }
}
