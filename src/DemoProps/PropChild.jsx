import React, { Component } from "react";

export default class PropChild extends Component {
  render() {
    return <div>PropChild: {this.props.children}</div>;
  }
}
