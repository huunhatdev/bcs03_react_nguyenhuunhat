import React, { Component } from "react";
import ChildComponent from "./Child.component";
import PropChild from "./PropChild";

//giao tiếp giữa các Component với nhau
export default class DemoProps extends Component {
  state = {
    userName: "Alice",
    age: 12,
    gender: "Female",
  };

  handleChangeName = (name) => {
    this.setState({
      userName: name,
    });
  };

  render() {
    return (
      <div>
        <ChildComponent
          username={this.state.userName}
          age={this.state.age}
          gender={this.state.gender}
          handleChangeName={this.handleChangeName}
        />
        <hr></hr>
        <h2>Demo PropsChild</h2>
        <PropChild>chelsea</PropChild>
      </div>
    );
  }
}
