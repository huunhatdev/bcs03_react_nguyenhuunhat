import React, { Component } from "react";

export default class ChildComponent extends Component {
  render() {
    // let [helloTitle] = this.props;
    return (
      <>
        <p className="text text-info">Tên: {this.props.username}</p>
        <p className="text text-info">Tuổi: {this.props.age}</p>
        <p className="text text-info">Giới tính: {this.props.gender}</p>
        <button
          className="btn btn-warning text-white"
          onClick={() => {
            this.props.handleChangeName("Minhhh");
          }}
        >
          Change Person
        </button>
      </>
    );
  }
}
