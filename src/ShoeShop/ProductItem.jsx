import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { product, handleAddToCart } = this.props;

    return (
      <div className="card-group col-3 mb-3">
        <div className="card">
          <img
            className="card-img-top"
            src={product.image}
            alt="Card image cap"
          />
          <div className="card-body text-left">
            <h4 className="card-title">{product.name}</h4>
            <p className="card-text text-danger">${product.price}</p>
            <button
              className="btn btn-info"
              onClick={() => {
                handleAddToCart(product);
              }}
            >
              Add to cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
