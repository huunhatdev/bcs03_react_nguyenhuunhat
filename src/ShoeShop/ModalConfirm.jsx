import React, { Component } from "react";
import { Modal } from "antd";

export default class ModalConfirm extends Component {
  render() {
    return (
      <Modal
        title="Bạn có muốn xoá sản phẩm này ???"
        visible={this.props.visible}
        onOk={() => {}}
        onCancel={() => {}}
      >
        <p>Some contents...</p>
      </Modal>
    );
  }
}
