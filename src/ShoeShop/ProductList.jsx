import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    let { productList, handleAddToCart } = this.props;

    return (
      <div className="container">
        <h1 className="bg-dark text-white">ShoeShop</h1>
        <div className="row">
          {productList.map((e, i) => {
            return (
              <ProductItem
                key={i}
                product={e}
                handleAddToCart={handleAddToCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
