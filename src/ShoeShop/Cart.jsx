import React, { Component } from "react";
import ModalConfirm from "./ModalConfirm";

export default class Cart extends Component {
  render() {
    return (
      <div className="container">
        <ModalConfirm visible={this.props.isOpenModal} />
        <h4>Cart</h4>
        <table className="table table-borderless">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Image</th>
              <th scope="col">Name</th>
              <th scope="col">Price</th>
              <th scope="col">Quantity</th>
              <th scope="col">Total</th>
              <th scope="col">Edit</th>
            </tr>
          </thead>
          <tbody>
            {this.props.product.map((e, i) => {
              return (
                <tr>
                  <th scope="row">{i + 1}</th>
                  <td className="col-1">
                    <img src={e.image} alt="" className="img-fluid" />
                  </td>
                  <td>{e.name}</td>
                  <td>{e.price}</td>
                  <td>
                    <button
                      className="btn btn-dark mr-2"
                      onClick={() => {
                        this.props.handleQuantity(e.id, -1);
                      }}
                    >
                      Giảm
                    </button>
                    {e?.quantity}
                    <button
                      className="btn btn-success ml-2"
                      onClick={() => {
                        this.props.handleQuantity(e.id, +1);
                      }}
                    >
                      Tăng
                    </button>
                  </td>
                  <td>{e.quantity * e.price}</td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={() => {
                        this.props.handleRemoveProduct(e.id);
                      }}
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
