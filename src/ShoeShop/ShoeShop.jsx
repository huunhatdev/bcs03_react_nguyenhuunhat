import React, { Component } from "react";
import { act } from "react-dom/test-utils";
import Cart from "./Cart";
import dataShoeShop from "./dataShoeShop.json";
import ProductList from "./ProductList";

export default class ShoeShop extends Component {
  state = {
    productList: dataShoeShop,
    cart: [],
    isOpenModal: false,
  };

  handleModal = (action) => {
    let isOpen = false;
    switch (action) {
      case "open":
        isOpen = true;
        break;
      case "close":
        isOpen = false;
        break;
    }
    this.setState({ isOpenModal: isOpen });
  };

  handleAddToCart = (product) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((e) => {
      return e.id === product.id;
    });
    if (index === -1) {
      let newProduct = { ...product, quantity: 1 };
      cloneCart.push(newProduct);
    } else {
      cloneCart[index].quantity += 1;
    }

    this.setState({
      cart: cloneCart,
    });
  };

  handleRemoveProduct = (id) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((e) => {
      return e.id === id;
    });
    if (index !== -1) {
      cloneCart.splice(index, 1);
      this.setState({
        cart: cloneCart,
      });
    }
  };

  handleQuantity = (id, value) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((e) => {
      return e.id === id;
    });
    if (cloneCart[index].quantity === 1 && value === -1) {
      this.handleModal("open");
      return;
    }
    if (index !== -1) {
      cloneCart[index].quantity += value;
    }

    !cloneCart[index].quantity && cloneCart.splice(index, 1);

    this.setState({
      cart: cloneCart,
    });
  };

  render() {
    return (
      <div>
        <ProductList
          productList={this.state.productList}
          handleAddToCart={this.handleAddToCart}
        />
        <hr />
        <h4>Số lượng giỏ hàng: {this.state.cart.length}</h4>
        {this.state.cart.length > 0 && (
          <Cart
            isOpenModal={this.state.isOpenModal}
            product={this.state.cart}
            handleRemoveProduct={this.handleRemoveProduct}
            handleQuantity={this.handleQuantity}
          />
        )}
      </div>
    );
  }
}
