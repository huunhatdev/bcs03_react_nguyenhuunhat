import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  render() {
    let isLogin = true;

    let handleLogin = () => {
      isLogin = true;
      console.log({ isLogin });
    };
    let handleLogout = () => {
      isLogin = false;
      console.log({ isLogin });
    };

    let renderContent = () => {
      if (isLogin) {
        return (
          <div>
            {" "}
            <p className="text text-success">Hello Name</p>
            <button className="btn btn-danger" onClick={handleLogout}>
              Logout
            </button>
          </div>
        );
      } else {
        return (
          <div>
            <p className="text text-warning">Please Login</p>

            <button className="btn btn-primary" onClick={handleLogin}>
              Login
            </button>
          </div>
        );
      }
    };

    return <div className="container mt-2">{renderContent()}</div>;

    // return <></>;
  }
}
