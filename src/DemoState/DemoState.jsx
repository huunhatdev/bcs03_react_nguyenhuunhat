import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    isLogin: true,
    username: "Nhat",
  };

  render() {
    let handleLogin = () => {
      this.setState({
        isLogin: true,
      });
    };
    let handleLogout = () => {
      this.setState({
        isLogin: false,
      });
    };

    let renderContent = () => {
      if (this.state.isLogin) {
        return (
          <div>
            {" "}
            <p className="text text-success">Hello {this.state.username}</p>
            <button className="btn btn-danger" onClick={handleLogout}>
              Logout
            </button>
          </div>
        );
      } else {
        return (
          <div>
            <p className="text text-warning">Please Login</p>
            <button className="btn btn-warning" onClick={handleLogin}>
              Login
            </button>
          </div>
        );
      }
    };

    return <div className="container mt-2">{renderContent()}</div>;

    // return <></>;
  }
}
