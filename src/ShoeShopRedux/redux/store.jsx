import { createStore } from "redux";
import { rootShoeShop } from "./reducers/rootReducer";

export const storeShoeShop = createStore(
  rootShoeShop,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
