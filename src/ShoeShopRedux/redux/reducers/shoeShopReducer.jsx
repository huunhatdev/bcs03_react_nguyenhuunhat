import { dataShoeShop } from "../../data";
import {
  ADD_TO_CART,
  OPEN_MODAL_CART,
  QUANTITY_PRODUCT,
  REMOVE_PRODUCT,
} from "../constants/constants";

const initialState = {
  productList: dataShoeShop,
  cart: [],
  isOpenModal: false,
};

export default (state = initialState, { type, payload }) => {
  let cloneCart;
  let index;
  switch (type) {
    case ADD_TO_CART:
      cloneCart = [...state.cart];
      index = cloneCart.findIndex((e) => {
        return e.id === payload.id;
      });
      if (index === -1) {
        let newProduct = { ...payload, quantity: 1 };
        cloneCart.push(newProduct);
      } else {
        cloneCart[index].quantity += 1;
      }
      state.cart = cloneCart;
      return { ...state };

    case REMOVE_PRODUCT:
      cloneCart = [...state.cart];
      index = cloneCart.findIndex((e) => {
        return e.id === payload;
      });
      if (index !== -1) {
        cloneCart.splice(index, 1);
      }
      state.cart = cloneCart;
      return { ...state };

    case QUANTITY_PRODUCT:
      cloneCart = [...state.cart];
      index = cloneCart.findIndex((e) => {
        return e.id === payload.id;
      });
      if (cloneCart[index].quantity === 1 && payload.value === -1) {
        state.isOpenModal = true;
        return { ...state };
      }
      if (index !== -1) {
        cloneCart[index].quantity += payload.value;
      }
      !cloneCart[index].quantity && cloneCart.splice(index, 1);
      state.cart = cloneCart;
      return { ...state };

    case OPEN_MODAL_CART:
      let isOpen = false;
      switch (payload) {
        case "open":
          state.isOpenModal = true;
          break;
        case "close":
          state.isOpenModal = false;
          break;
      }
      return { ...state };

    default:
      return state;
  }
};
