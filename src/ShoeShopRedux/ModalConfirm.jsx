import React, { Component } from "react";
import { Modal } from "antd";
import { connect } from "react-redux";
import { OPEN_MODAL_CART } from "./redux/constants/constants";

class ModalConfirm extends Component {
  render() {
    return (
      <Modal
        title="Bạn có muốn xoá sản phẩm này ???"
        visible={this.props.visible}
        onOk={() => {
          this.props.handleModal("close");
        }}
        onCancel={() => {
          this.props.handleModal("close");
        }}
      >
        <p>Some contents...</p>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    visible: state.shoeShopReducer.isOpenModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleModal: (action) => {
      dispatch({
        type: OPEN_MODAL_CART,
        payload: action,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalConfirm);
