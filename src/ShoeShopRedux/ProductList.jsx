import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";

class ProductList extends Component {
  render() {
    return (
      <div className="container">
        <h1 className="bg-dark text-white">ShoeShop</h1>
        <div className="row">
          {this.props.productList?.map((e, i) => {
            return (
              <ProductItem
                key={i}
                product={e}
                // handleAddToCart={this.props?.handleAddToCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    productList: state.shoeShopReducer.productList,
  };
};

const mapDispatchToProps = (dispatch) => {};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);
