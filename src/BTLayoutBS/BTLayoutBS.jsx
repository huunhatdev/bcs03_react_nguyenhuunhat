import React, { Component } from "react";
import Banner from "./Banner";
import Footer from "./Footer";
import Header from "./Header";
import Item from "./Item";
import "./srcLayout/css/styles.css";
import "./srcLayout/js/scripts.js";

export default class BTLayoutBS extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <section class="pt-4">
          <div class="container px-lg-5">
            <div class="row gx-lg-5">
              <Item />
              <Item />
              <Item />
              <Item />
              <Item />
              <Item />
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}
