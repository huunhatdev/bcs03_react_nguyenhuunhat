import logo from "./logo.svg";
import "./App.css";
// import DemoClass from "./DemoComponent/DemoClass";
// import DemoFunction from "./DemoComponent/DemoFunction";
// import BTLayoutBS from "./BTLayoutBS/BTLayoutBS";
// import DataBind from "./DataBind/DataBind";
// import EventBinding from "./EventBinding/EventBinding";
// import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";
import DemoState from "./DemoState/DemoState";
import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";
import DemoProps from "./DemoProps/DemoProps";
import ShoeShop from "./ShoeShop/ShoeShop";
import "antd/dist/antd.css";
import DemoMiniRedux from "./DemoMiniRedux/DemoMiniRedux";
import ShoeShopRedux from "./ShoeShopRedux/ShoeShopRedux";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction /> */}
      {/* <BTLayoutBS /> */}
      {/* <DataBind /> */}
      {/* <EventBinding /> */}
      {/* <ConditionalRendering /> */}
      {/* <DemoState /> */}
      {/* <BaiTapChonXe /> */}
      {/* <DemoProps /> */}
      {/* <ShoeShop /> */}
      {/* <DemoMiniRedux /> */}
      <ShoeShopRedux />
    </div>
  );
}

export default App;
