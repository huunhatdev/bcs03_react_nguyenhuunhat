import React, { Component } from "react";

export default class DataBind extends Component {
  styleText = {
    backgroundColor: "black",
    color: "white",
  };
  render() {
    let userName = "James";
    let userNameClass = "text-success";
    let renderIntro = () => {
      return <p className="text-warning bg-danger">Việt Nam vô địch</p>;
    };

    return (
      <div style={this.styleText}>
        Hello {userName} {renderIntro()}
      </div>
    );
  }
}
