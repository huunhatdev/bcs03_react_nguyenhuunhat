import React, { Component } from "react";

export default class EventBinding extends Component {
  handleSayHi = (userName) => {
    console.log(`HI ${userName}`);
  };

  render() {
    return (
      <>
        <button
          className="btn btn-success"
          onClick={() => {
            this.handleSayHi("Nick");
          }}
        >
          CLick Me!
        </button>
      </>
    );
  }
}
