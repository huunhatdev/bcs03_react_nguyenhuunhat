import React, { Component } from "react";
import { connect } from "react-redux";
import { tangSoLuongAction } from "./redux/action";

class DemoMiniRedux extends Component {
  render() {
    return (
      <div>
        <button className="btn btn-secondary m-2">-</button>
        {this.props.value}
        <button
          className="btn btn-success m-2"
          onClick={() => {
            this.props.handleTangNumber();
          }}
        >
          +
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    value: state.numberReducer.number,
  };
};

let mapDispacthToProps = (dispatch) => {
  return {
    handleTangNumber: () => {
      dispatch(tangSoLuongAction(12));
    },
  };
};

//connect(state lấy về)(đối tượng áp dụng)
export default connect(mapStateToProps, mapDispacthToProps)(DemoMiniRedux);
