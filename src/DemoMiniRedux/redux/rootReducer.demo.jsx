import { combineReducers } from "redux";
import { numberReducer } from "./NumberReducer";

export const rootReducerDemo = combineReducers({
  numberReducer,
});

//key quản lý reducer => dùng key để lấy giá trị từ các key trong initialState
