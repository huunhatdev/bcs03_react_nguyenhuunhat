//khởi tạo giá trị store (~ state)

import { TANG_SO_LUONG } from "./const";

const initialState = {
  number: 0,
  total: 10,
};

export const numberReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TANG_SO_LUONG:
      state.number += payload;
      return { ...state };
    default:
      return state;
  }
};
